from pdf2image import convert_from_path  # NOTE(Mr.Fox): Convert PDF to images
import dearpygui.dearpygui as dpg
import webbrowser
import img2pdf  # NOTE(Mr.Fox): Convert Images to PDF
import json
import os
import time

### Write Json File_IMG (In Cache Folder) ###


def write_json_img(data, file_name="../Cache/File name_img.json"):
    with open(file_name, "w") as write:
        json.dump(data, write, indent=4)

### Write Json File_IMG (In Cache Folder) ###


def write_json_pdf(data, file_name="../Cache/File name_pdf.json"):
    with open(file_name, "w") as write:
        json.dump(data, write, indent=4)

### Read Json File_IMG (In Cache Folder) ###


def read_json_img(file_name="../Cache/File name_img.json"):
    with open(file_name) as read:
        data = json.load(read)
        print(data["selections"])
        for index in data["selections"]:
            dpg.add_text(index, parent="source_img")
            dpg.add_selectable(label=str(index), parent="img")

### Read Json File_PDF (In Cache Folder) ###


def read_json_pdf(file_name="../Cache/File name_pdf.json"):
    with open(file_name) as read:
        data = json.load(read)
        print(data["selections"])
        for index in data["selections"]:
            dpg.add_text(index, parent="source_pdf")
            dpg.add_selectable(label=str(index), parent="pdf")

### Reload All Data In Cache Folder ###


def Retrieving_Information():
    try:
        read_json_img()
    except Exception as e:
        print("Error!" + str(e))
    try:
        read_json_pdf()
    except Exception as e:
        print("Error!" + str(e))

### Clear Data On Convertor Window & Delete All Data In Cache Folder ###


def Clear_data():
    try:
        os.remove("../Cache/File name_img.json")
    except Exception as e:
        print("Error!" + str(e))
    try:
        os.remove("../Cache/File name_pdf.json")
    except Exception as e:
        print("Error!" + str(e))
    dpg.delete_item("pdf", children_only=True)
    dpg.delete_item("img", children_only=True)
    dpg.delete_item("source_pdf", children_only=True)
    dpg.delete_item("source_img", children_only=True)

### Close Software ###


def Call_Exit():
    os._exit(os.EX_OK)

### Open Image Files ###


def Call_Import_File_Img(sender, app_data):
    File_Name = []
    File_Address = []
    print("Sender: ", sender)
    print("App Data: ", app_data)
    write_json_img(app_data)
    read_json_img()

### Open PDF Files ###


def Call_Import_File_Pdf(sender, app_data):
    File_Name = []
    File_Address = []
    print("Sender: ", sender)
    print("App Data: ", app_data)
    write_json_pdf(app_data)
    read_json_pdf()

### Open Themes Editor ###


def Call_Theme():
    dpg.show_style_editor()

### Open Font Editor ###


def Call_Font():
    dpg.show_font_manager()

### Open My Source code On Glitlab ###


def Call_Source_Code():
    get_url = webbrowser.open('https://gitlab.com/mr-fox-h/FDF.git')

### Open My Website On Blog.ir ###


def Call_Site():
    get_url = webbrowser.open('https://mr-fox.blog.ir')


def Call_Gitlab():
    get_url = webbrowser.open('https://gitlab.com/mr-fox-h/')

### Take Name ###


def Call_Get_Name_Img(sender):
    global name
    name = str(dpg.get_value(sender))
    print(name)

### Take Address ###


def Call_Get_Address_Img(sender):
    global address
    address = dpg.get_value(sender)
    print(address)

### Export To PDF ###


def Call_Export_Node_To_PDF():
    with open("../Cache/File name_img.json") as read:
        data = json.load(read)
        print(data["selections"])
        value = []
        for index, item in data["selections"].items():
            value.append(item)
            print(value)
        with open("/" + str(address) + "/" + str(name) + ".pdf", "wb") as f:
            f.write(img2pdf.convert(value))

### Take Name ###


def Call_Get_Name_PDF(sender):
    global name_PDF
    name_PDF = str(dpg.get_value(sender))
    print(name_PDF)

### Take Address ###


def Call_Get_Address_PDF(sender):
    global address_PDF
    address_PDF = dpg.get_value(sender)
    print(address_PDF)

### Export To Images ###


def Call_Export_Node_To_Images():
    with open("../Cache/File name_pdf.json") as read:
        data = json.load(read)
        print(data["selections"])
        value = []
        for index, item in data["selections"].items():
            value.append(item)
            print(value)
        # TODO(Mr.Fox): Read data in chache
        os.mkdir(address_PDF + '/' + name_PDF)
        count = 1
        for list_object in value:
            print("List: " + list_object)
            pages = convert_from_path(list_object)
            for page in pages:  # NOTE(Mr.Fox): Saving pages in jpeg format
                page.save(str(address_PDF) + '/' + str(name_PDF) +
                          '/' + str(count) + '.jpg', 'JPEG')
                count += 1

### Show & Clear Text in How To Use Window ###


def Show_Tutorial():
    dpg.delete_item("tutorial_text", children_only=True)
    Name = dpg.get_value("pages")
    if Name == "Basic":
        dpg.add_text("Introduction & Basics", bullet=True,
                     color=(0, 255, 255, 255), parent="tutorial_text")
        dpg.add_separator(parent="tutorial_text")
        dpg.add_text("Hi people!", parent="tutorial_text")
        dpg.add_text("Welcome to Fox PTPDF. Fox PTPDF is a free and open source software that you can convert any image to PDF and reverse.",
                     parent="tutorial_text", wrap=700)
        dpg.add_text("This software made by Python language! Why did I use Python?",
                     parent="tutorial_text", wrap=700)
        dpg.add_text("Because it's easy to use and I found some cool document and library looks like:",
                     parent="tutorial_text", wrap=700)
        dpg.add_text("Dearpygui", bullet=True, parent="tutorial_text",
                     color=(255, 255, 150, 255), wrap=700)
        dpg.add_text("pdf2image", bullet=True, parent="tutorial_text",
                     color=(255, 255, 150, 255), wrap=700)
        dpg.add_text("img2pdf", bullet=True, parent="tutorial_text",
                     color=(255, 255, 150, 255), wrap=700)
        dpg.add_text("json", bullet=True, parent="tutorial_text",
                     color=(255, 255, 150, 255), wrap=700)
        dpg.add_text("os", bullet=True, parent="tutorial_text",
                     color=(255, 255, 150, 255), wrap=700)
        dpg.add_text("Fox PTPDF is easy to use, but if you found a bug or something like that, please add a Issues on my Gitlab page.",
                     parent="tutorial_text", wrap=700)
        dpg.add_text("You can Find my Gitlab page in Help tab in menu bar.",
                     parent="tutorial_text", wrap=700)
    elif Name == "Installation":
        dpg.add_text("Installation", bullet=True, color=(
            0, 255, 255, 255), parent="tutorial_text")
        dpg.add_separator(parent="tutorial_text")
        dpg.add_text("If you can read this note it's mean you installed Fox PTPDF, but if have some problem about the functionality of software, you need to install all library with your hands. First you have to install pip3 (IF YOU DO NOT HAVE PIP3):", parent="tutorial_text", wrap=700)
        dpg.add_text("Open your terminal and write all of them (IN LINUX OS, NOT WINDOWS OS):",
                     parent="tutorial_text", wrap=700)
        dpg.add_text("$ sudo apt-get update", bullet=True,
                     parent="tutorial_text", color=(255, 255, 150, 255), wrap=700)
        dpg.add_text("$ sudo apt-get install poppler", bullet=True,
                     parent="tutorial_text", color=(255, 255, 150, 255), wrap=700)
        dpg.add_text("$ sudo apt-get -y install python3-pip", bullet=True,
                     parent="tutorial_text", color=(255, 255, 150, 255), wrap=700)
        dpg.add_text("$ pip3 --version", bullet=True,
                     parent="tutorial_text", color=(255, 255, 150, 255), wrap=700)
        dpg.add_text("Next you need to install some packages with pip3 (YOU CAN DO THAT IN LINUX AND WINDOWS OS):",
                     parent="tutorial_text", wrap=700)
        dpg.add_text("$ pip3 install dearpygui", bullet=True,
                     parent="tutorial_text", color=(255, 255, 150, 255), wrap=700)
        dpg.add_text("$ pip3 install pycopy-webbrowser", bullet=True,
                     parent="tutorial_text", color=(255, 255, 150, 255), wrap=700)
        dpg.add_text("$ pip3 install pdf2image", bullet=True,
                     parent="tutorial_text", color=(255, 255, 150, 255), wrap=700)
        dpg.add_text("$ pip3 install img2pdf", bullet=True,
                     parent="tutorial_text", color=(255, 255, 150, 255), wrap=700)
        dpg.add_text("Now you have to restart this program. Have a nice day.",
                     parent="tutorial_text", wrap=700)
    elif Name == "How To Use?":
        dpg.add_text("How To Use?", bullet=True, color=(
            0, 255, 255, 255), parent="tutorial_text")
        dpg.add_separator(parent="tutorial_text")
        dpg.add_text("Now you know basics and you know how to install it on your computer, but how to use Fox PTPDF?",
                     parent="tutorial_text", wrap=700)
        dpg.add_text("For example I want to convert a image to PDF.",
                     parent="tutorial_text", wrap=700)
        dpg.add_text("Click on File and select Open Images in menu bar. You will see a File Manager window on screen, select your image or images and clik OK or press Enter on your keyboard. If you want to cancel it, press cancel button. Easy!", parent="tutorial_text", wrap=700)
        dpg.add_text("Now you imported your images in Fox PTPDF. Go to View and select Convertor in menu bar.",
                     parent="tutorial_text", wrap=700)
        dpg.add_text("Go to Basic and open Images -> PDF tab, then open Images and Export Settings. Enter a name and Address in Export Settings, then press Convert Button.", parent="tutorial_text", wrap=700)
        dpg.add_text("Now you can see your PDF in your location.",
                     parent="tutorial_text", wrap=700)
    elif Name == "Ajustments":
        dpg.add_text("Ajustments", bullet=True, color=(
            0, 255, 255, 255), parent="tutorial_text")
        dpg.add_separator(parent="tutorial_text")
        dpg.add_text("Not Yet.", parent="tutorial_text", wrap=700)
    elif Name == "Readers":
        dpg.add_text("Readers", bullet=True, color=(
            0, 255, 255, 255), parent="tutorial_text")
        dpg.add_separator(parent="tutorial_text")
        dpg.add_text("Not Yet.", parent="tutorial_text", wrap=700)
    else:
        pass


dpg.create_context()  # NOTE(Mr.Fox): Start my GUI software here :)

### Add A Font Registry ###
with dpg.font_registry():

    ### First argument ids the path to the .ttf or .otf file ###
    Font_1 = dpg.add_font(
        "../Fonts/InconsolataZi4varlvarquRegular-42gD.otf", 14)
    Font_2 = dpg.add_font("../Fonts/Ubuntu-n1M0.ttf", 14)
    Font_3 = dpg.add_font("../Fonts/JetBrainsMono-SemiBold.ttf", 15)

    ### Set font of specific widget ###
    dpg.bind_font(Font_1)
    dpg.bind_font(Font_2)
    dpg.bind_font(Font_3)

### Themes Windows ###
with dpg.window(width=600, pos=[30, 30], label="change_menu_bg", show=False):
    dpg.add_text(
        "this work if bind global theme, style_editor makes no effect if bind theme")
    dpg.add_input_intx(default_value=[255, 0, 0, 255], min_value=0, max_value=255, width=200, callback=lambda s, a, u: [
                       dpg.configure_item("MenuBarBg_", value=a), print(a)])

### About Window ###
with dpg.window(label="About", height=550, width=530, min_size=[528, 458], modal=True, show=False, no_scrollbar=True, pos=(500, 100), id="explanation") as About:
    dpg.add_text("Fox PTPDF", bullet=True, color=(0, 255, 255, 255))
    dpg.add_text("Version 0.5.2", bullet=True, color=(0, 255, 255, 255))
    dpg.add_separator()
    dpg.add_text("Convert photo to PFD and reverse. It's to easy.",
                 bullet=True, color=(0, 255, 255, 255))
    dpg.add_text(
        "Welcome to Fox PTPDF. Fox PTPDF is a free and open source software that you can convert any image to PDF.", wrap=500)
    dpg.add_text("If you do not know how to use Fox PTPDF, you can go to \'Help\' -> \'How to use\' and start learning about Fox PTPDF.\n It's not difficult.\n", wrap=500)
    with dpg.child_window(no_scrollbar=True):
        dpg.add_text("MIT License", wrap=500, bullet=True,
                     color=(0, 255, 255, 255))
        dpg.add_text("Copyright (c) 2021 Mr-Fox-h", wrap=500,
                     bullet=True, color=(0, 255, 255, 255))
        dpg.add_text("Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the \"Software\"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:", wrap=500)
        dpg.add_text(
            "The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.", wrap=500)
        dpg.add_text("THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.", wrap=500)

### How To Use Window ###
with dpg.window(label="How to use", height=500, width=910, min_size=[910, 500], show=False, id="tutorial", no_scrollbar=True) as Tutorial:
    tutorials = ["Basic", "Installation",
                 "How To Use?", "Ajustments", "Readers"]
    with dpg.table(header_row=False, resizable=True):
        dpg.add_table_column(init_width_or_weight=0.2)
        dpg.add_table_column(init_width_or_weight=0.8)
        with dpg.table_row():
            with dpg.child_window():
                dpg.add_text("Tutorials")
                dpg.add_separator()
                dpg.add_radio_button(
                    items=tutorials, callback=Show_Tutorial, tag="pages")
            with dpg.child_window(tag="tutorial_text"):
                pass

### Reader Window ###
# TODO(Mr.Fox): Make reader functional
with dpg.window(label="Reader", tag="reader", width=900, height=550, show=False, min_size=(540, 600), no_scrollbar=True) as reader:
    with dpg.menu_bar():  # Menu Bar For Reader Window ###
        with dpg.menu(label="File"):
            dpg.add_menu_item(label="Import")  # TODO(Mr.Fox): Add file dialog
            # TODO(Mr.Fox): Clear all data in Reader window
            dpg.add_menu_item(label="Clear Data")

    ### Child Windows For Reader ###
    with dpg.table(header_row=False, resizable=True):
        dpg.add_table_column()
        dpg.add_table_column()
        dpg.add_table_column(init_width_or_weight=0.5)
        with dpg.table_row():
            with dpg.child_window():
                dpg.add_text("Image")
                dpg.add_separator()
            with dpg.child_window():
                dpg.add_text("Texts")
                dpg.add_separator()
            with dpg.child_window():
                dpg.add_text("Files")
                dpg.add_separator()


### Import Image Files ###
with dpg.file_dialog(label="Image Files", directory_selector=False, width=700, height=300, modal=True, show=False, callback=Call_Import_File_Img, tag="file_dialog_tag_img"):
    dpg.add_file_extension("")
    dpg.add_file_extension(".*", color=(255, 150, 150, 255))
    dpg.add_file_extension(".png", color=(11, 97, 247, 255))
    dpg.add_file_extension(".jpg", color=(11, 247, 50, 255))

### Import PDF Files ###
with dpg.file_dialog(label="PDF Files", directory_selector=False, width=700, height=300, modal=True, show=False, callback=Call_Import_File_Pdf, tag="file_dialog_tag_pdf"):
    dpg.add_file_extension("")
    dpg.add_file_extension(".*", color=(255, 150, 150, 255))
    dpg.add_file_extension(".pdf", color=(11, 97, 247, 255))

### Tab Bar ###
with dpg.viewport_menu_bar():  # NOTE(Mr.Fox): Menu bar with items
    with dpg.menu(label="File"):  # NOTE(Mr.Fox): File Menu
        with dpg.menu(label="Import"):
            dpg.add_menu_item(label="Images",
                              callback=lambda: dpg.show_item("file_dialog_tag_img"))
            dpg.add_menu_item(
                label="PDFs", callback=lambda: dpg.show_item("file_dialog_tag_pdf"))

        # NOTE(Mr.Fox): Read all data in Cache Folder
        dpg.add_menu_item(label="Retrieving Information",
                          callback=Retrieving_Information)

        # NOTE(Mr.Fox): Clear all data in Convertor Window
        dpg.add_menu_item(label="Clear Data", callback=Clear_data)
        dpg.add_menu_item(label="Exit", callback=Call_Exit)
    with dpg.menu(label="Edit"):  # NOTE(Mr.Fox): Edit Menu
        with dpg.menu(label="Custom"):
            dpg.add_menu_item(label="Themes", callback=Call_Theme)
            dpg.add_menu_item(label="Fonts", callback=Call_Font)
        dpg.add_menu_item(label="Settings")
    with dpg.menu(label="View"):  # NOTE(Mr.Fox): View Menu
        dpg.add_menu_item(
            label="Convertor", callback=lambda: dpg.configure_item("convertor", show=True))
        dpg.add_menu_item(label="Reader", callback=lambda: dpg.configure_item(
            "reader", show=True))  # TODO(Mr.Fox): Make File_Reade window to show logs
    with dpg.menu(label="Help"):  # NOTE(Mr.Fox): Help Menu
        dpg.add_menu_item(
            label="How to use", callback=lambda: dpg.configure_item("tutorial", show=True))
        with dpg.menu(label="Document"):
            # Note(Mr.Fox): Lead him/her to my website
            dpg.add_menu_item(label="Website", callback=Call_Site)
            # NOTE(Mr.Fox): Lead people to my Gitlab page
            dpg.add_menu_item(label="Source", callback=Call_Source_Code)
            # NOTE(Mr.Fox): Lead people to my Gitlab page
            dpg.add_menu_item(label="Gitlab", callback=Call_Gitlab)
        dpg.add_menu_item(label="About", callback=lambda: dpg.configure_item(
            "explanation", show=True))
    with dpg.menu(label="Developer's Tools"):
        dpg.add_menu_item(label="Dearpygui Theme Editor", callback=Call_Theme)
        dpg.add_menu_item(label="Dearpygui Font Editor", callback=Call_Font)
        dpg.add_menu_item(label="Dearpygui Debug", callback=lambda: dpg.configure_item(
            dpg.show_debug(), show=True))

### Convertor Window ###
with dpg.window(label="Convertor", tag="convertor", show=False, no_scrollbar=True, width=950, height=600) as Fox_PTPDF:
    with dpg.menu_bar():  # Menu Bar For Reader Window ###
        with dpg.menu(label="Clear"):
            # Note(Mr.Fox): Clear all data in Convertor Window
            dpg.add_menu_item(label="Clear Data", callback=Clear_data)
            with dpg.tooltip(dpg.last_item()):
                dpg.add_text("Clear & Delete")
                dpg.add_separator()
                dpg.add_text(
                    "Clear all data in Convertor window & delete all data in cache folder")
    # NOTE(Mr.Fox): I divide this window to two window {Left: Files imported, Right: Tools}
    with dpg.table(header_row=False, resizable=True):
        dpg.add_table_column(init_width_or_weight=0.2)
        dpg.add_table_column(init_width_or_weight=0.8)
        with dpg.table_row():
            with dpg.child_window():
                dpg.add_text("List")
                dpg.add_separator()
                with dpg.tab_bar():  # NOTE(Mr.Fox): Show images and files
                    with dpg.tab(label="Image Files", tag="source_img"):
                        with dpg.table(header_row=False):
                            dpg.add_table_column(init_width_or_weight=0.2)
                            dpg.add_table_column()

                    with dpg.tab(label="PDF Files", tag="source_pdf"):
                        with dpg.table(header_row=False):
                            dpg.add_table_column(init_width_or_weight=0.2)
                            dpg.add_table_column()

            with dpg.child_window(no_scrollbar=True):  # NOTE(Mr.Fox): Tools frame
                dpg.add_text("Tools", show_label=False)
                dpg.add_separator()
                with dpg.table(header_row=True, resizable=True):
                    dpg.add_table_column(label="Basic")
                    dpg.add_table_column(label="Advance")
                    with dpg.table_row():
                        with dpg.child_window():  # NOTE(Mr.Fox): Collapsing Header for Images to PDF
                            dpg.add_collapsing_header(
                                label="Images -> PDF", tag="images to pdf")
                            with dpg.tree_node(label="Images", parent="images to pdf"):
                                with dpg.child_window(height=120, tag="img"):
                                    with dpg.tooltip("img"):
                                        dpg.add_text("your images")
                            with dpg.tree_node(label="Eport Settings", parent="images to pdf"):
                                with dpg.table(header_row=False):
                                    dpg.add_table_column()
                                    dpg.add_table_column()
                                    with dpg.table_row():
                                        dpg.add_input_text(
                                            label="Name", hint="Name", tag="name")
                                        dpg.add_input_text(
                                            label="Address", hint="Address", tag="address")
                                        dpg.set_item_callback(
                                            "name", Call_Get_Name_Img)
                                        dpg.set_item_callback(
                                            "address", Call_Get_Address_Img)
                                    with dpg.table_row():
                                        # NOTE(Mr.Fox): Collapsing Header for PDF to Images
                                        dpg.add_button(
                                            label="Convert", callback=Call_Export_Node_To_PDF)
                                        with dpg.tooltip(dpg.last_item()):
                                            dpg.add_text("Export Buutton")
                                            dpg.add_separator()
                                            dpg.add_text("export image to PDF")
                            dpg.add_collapsing_header(
                                label="PDF -> Images", tag="pdf to images")
                            with dpg.tree_node(label="PDF Files", parent="pdf to images"):
                                with dpg.child_window(height=120, tag="pdf"):
                                    with dpg.tooltip("pdf"):
                                        dpg.add_text("your pdf files")
                            with dpg.tree_node(label="Export Settings", parent="pdf to images"):
                                with dpg.table(header_row=False):
                                    dpg.add_table_column()
                                    dpg.add_table_column()
                                    with dpg.table_row():
                                        dpg.add_input_text(
                                            label="Name", hint="Name", tag="name_pdf")
                                        dpg.add_input_text(
                                            label="Address", hint="Address", tag="address_pdf")
                                        dpg.set_item_callback(
                                            "name_pdf", Call_Get_Name_PDF)
                                        dpg.set_item_callback(
                                            "address_pdf", Call_Get_Address_PDF)
                                    with dpg.table_row():
                                        # NOTE(Mr.Fox): Collapsing Header for Images to PDF
                                        dpg.add_button(
                                            label="Convert", callback=Call_Export_Node_To_Images)
                                        with dpg.tooltip(dpg.last_item()):
                                            dpg.add_text("Export Buutton")
                                            dpg.add_separator()
                                            dpg.add_text(
                                                "export PDF or PDFs to images")
                        with dpg.child_window():  # TODO(Mr.Fox): Add OpenCV Tools
                            dpg.add_text("Coming soon")
### Theme ###
with dpg.theme() as container_theme:

    with dpg.theme_component(dpg.mvAll):
        ### Static Theme ###
        dpg.add_theme_color(dpg.mvThemeCol_ModalWindowDimBg, (11, 29, 20, 70),
                            category=dpg.mvThemeCat_Core)  # TODO(Mr.Fox): It's doesn't work why?
        dpg.add_theme_color(dpg.mvThemeCol_TitleBg,
                            (11, 29, 36), category=dpg.mvThemeCat_Core)
        dpg.add_theme_color(dpg.mvThemeCol_Text, (255, 255,
                            255), category=dpg.mvThemeCat_Core)
        dpg.add_theme_color(dpg.mvThemeCol_WindowBg,
                            (11, 29, 40), category=dpg.mvThemeCat_Core)
        dpg.add_theme_color(dpg.mvThemeCol_ChildBg,
                            (3, 20, 25), category=dpg.mvThemeCat_Core)
        dpg.add_theme_color(dpg.mvThemeCol_PopupBg,
                            (11, 29, 40), category=dpg.mvThemeCat_Core)
        dpg.add_theme_color(dpg.mvThemeCol_FrameBg,
                            (91, 124, 133), category=dpg.mvThemeCat_Core)
        dpg.add_theme_color(dpg.mvThemeCol_TextDisabled,
                            (243, 244, 252, 150), category=dpg.mvThemeCat_Core)
        dpg.add_theme_color(dpg.mvThemeCol_MenuBarBg, [
                            0, 35, 57, 255], category=0, label="MenuBarBg", tag="MenuBarBg_")
        dpg.add_theme_color(dpg.mvThemeCol_Button,
                            (0, 35, 57), category=dpg.mvThemeCat_Core)
        dpg.add_theme_color(dpg.mvThemeCol_CheckMark,
                            (0, 35, 57), category=dpg.mvThemeCat_Core)
        dpg.add_theme_color(dpg.mvThemeCol_Header,
                            (11, 29, 36), category=dpg.mvThemeCat_Core)
        dpg.add_theme_color(dpg.mvThemeCol_ResizeGrip,
                            (59, 157, 195), category=dpg.mvThemeCat_Core)
        dpg.add_theme_color(dpg.mvThemeCol_Tab, (11, 29, 36),
                            category=dpg.mvThemeCat_Core)
        dpg.add_theme_color(dpg.mvThemeCol_TableHeaderBg,
                            (11, 29, 36), category=dpg.mvThemeCat_Core)

        ### Hovered Theme ###
        dpg.add_theme_color(dpg.mvThemeCol_ResizeGripHovered,
                            (59, 157, 195, 50), category=dpg.mvThemeCat_Core)
        dpg.add_theme_color(dpg.mvThemeCol_ButtonHovered,
                            (59, 157, 195, 50), category=dpg.mvThemeCat_Core)
        dpg.add_theme_color(dpg.mvThemeCol_HeaderHovered,
                            (59, 157, 195, 50), category=dpg.mvThemeCat_Core)
        dpg.add_theme_color(dpg.mvThemeCol_TabHovered,
                            (59, 157, 195, 50), category=dpg.mvThemeCat_Core)

        ### Active Theme ###
        dpg.add_theme_color(dpg.mvThemeCol_ResizeGripActive,
                            (59, 157, 195, 200), category=dpg.mvThemeCat_Core)
        dpg.add_theme_color(dpg.mvThemeCol_HeaderActive,
                            (59, 157, 195), category=dpg.mvThemeCat_Core)
        dpg.add_theme_color(dpg.mvThemeCol_TitleBgActive,
                            (59, 157, 195), category=dpg.mvThemeCat_Core)
        dpg.add_theme_color(dpg.mvThemeCol_ButtonActive,
                            (59, 157, 195), category=dpg.mvThemeCat_Core)
        dpg.add_theme_color(dpg.mvThemeCol_TabActive,
                            (59, 157, 195), category=dpg.mvThemeCat_Core)

        ### Style Theme ###
        dpg.add_theme_style(dpg.mvStyleVar_FrameRounding,
                            5, category=dpg.mvThemeCat_Core)
        dpg.add_theme_style(dpg.mvStyleVar_ScrollbarSize,
                            10, category=dpg.mvThemeCat_Core)
        dpg.add_theme_style(dpg.mvStyleVar_WindowRounding,
                            8, category=dpg.mvThemeCat_Core)
        dpg.add_theme_style(dpg.mvStyleVar_PopupRounding,
                            5, category=dpg.mvThemeCat_Core)
        dpg.add_theme_style(dpg.mvStyleVar_ChildRounding,
                            10, category=dpg.mvThemeCat_Core)
        dpg.add_theme_style(dpg.mvStyleVar_ChildRounding,
                            10, category=dpg.mvThemeCat_Core)
        dpg.add_theme_style(dpg.mvStyleVar_WindowTitleAlign,
                            0.5, 0.5, category=dpg.mvThemeCat_Core)
        dpg.add_theme_style(dpg.mvStyleVar_WindowBorderSize,
                            False, category=dpg.mvThemeCat_Core)
        # dpg.add_theme_style(dpg.mvStyleVar_TabBorderSize, True, category=dpg.mvThemeCat_Core)

    with dpg.theme_component(dpg.mvInputInt):
        dpg.add_theme_style(dpg.mvStyleVar_FrameRounding,
                            5, category=dpg.mvThemeCat_Core)

dpg.bind_item_theme(Fox_PTPDF, container_theme)
dpg.bind_item_theme(About, container_theme)
dpg.bind_item_theme(Tutorial, container_theme)
dpg.bind_item_theme(reader, container_theme)
dpg.bind_theme(container_theme)

dpg.create_viewport(title="Fox PTPDF")
dpg.setup_dearpygui()
dpg.show_viewport()
dpg.start_dearpygui()
dpg.destroy_context()
