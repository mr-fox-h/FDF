# Fox PTPDF🦊

## Introduction

Hi people!

Welcome to **Fox PTPDF**. **Fox PTPDF** is a **free** and **open source** software that you can convert any **image** to **PDF** and reverse.

## How to install Fox PTPDF?🤔

It's easy to install! First you have to install [python language](https://www.python.org/), then install **pip3** (IF YOU DO NOT HAVE PIP3):

```
$ sudo apt-get update
$ sudo apt-get -y install python3-pip
$ pip3 --version
```

Next you need to install some packages with **pip3**:

```
$ pip3 install dearpygui
$ pip3 install pycopy-webbrowser
$ pip3 install pdf2image
$ pip3 install img2pdf
```

## How to use Fox PTPDF?🤔

It's easy to use! Run **Fox PTPDF.py** file with this command:

```
$ python3 Fox\ PTPDF.py
```

Or you can use this command. No different:

```
$ python3 "Fox PTPDF.py"
```

Then you will see a black screen on your monitor.

![First Step](Images/1.png)

Click on **View** and select **Convertor**. You will see convertor window, but you can not do anything! why? Because you need to import some images in **Fox PTPDF**.

![Second Step](Images/2.png)

![Third Step](Images/3.png)

Click on **File** and select your image or images and press **_Enter_** on your keyboard or press **_Ok_** to import your image or images in **Fox PTPDF**.

![Fourth Step](Images/4.png)

![Fifth Step](Images/5.png)

Now you can see your image name in **Convertor** window.

![Sixth Step](Images/6.png)

Go to **Basic** and open **Images -> PDF** tab, then open **Images** and **Export Settings**. Enter a name and Address in **Export Settings**, then press **Convert** Button.

![Seventh Step](Images/7.png)

Now you can see your PDF in your location 😁.

![Eighth Step](Images/8.png)

## Future🗃️

In future, I will add and change something amazing and cool.

- I will add photo editor
- I will add executable files for Window, Linux and Macintosh OS
- I will add a simple documents (Almost done)
- I will fix Exported tab
- I will add a **Text Detector**

## Request from you🌍

Thank for using my software and I have some request for you:

1. Please share my software with your friends and other people.
2. please add some issues for me if you see some problem.
3. please add some suggestion about **Fox PTPDF** if you have any suggestion.

thank you👐

## Version📢

**Fox PTPDF** by **_Mr.Fox_**

Version **0.5.2**
